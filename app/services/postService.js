const postRepository = require("../repositories/postRepository");

module.exports = {
  async list() {
    const posts = await postRepository.findAll();

    return {
      data: posts,
    };
  },

  create(requestBody) {
    return postRepository.create(requestBody);
  },
};
