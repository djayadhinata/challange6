const userRepository = require("../repositories/userRepository");
const { encryptPassword } = require("../controllers/api/v1/auth");

module.exports = {
  async create(requestBody) {
    const email = requestBody.body.email;
    const pass = requestBody.body.password;

    const encryptedPassword = await encryptPassword(pass);

    return userRepository.create({ email, encryptedPassword });
  },
};
