const { user } = require("../models");

module.exports = {
  create(requestBody) {
    return user.create(requestBody);
  },
};
