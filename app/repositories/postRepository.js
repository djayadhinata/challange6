const { post } = require("../../config/routes");
const { Post } = require("../models");

module.exports = {
  findAll() {
    return Post.findAll();
  },

  create(requestBody) {
    return Post.create(requestBody);
  },
};
