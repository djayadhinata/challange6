const userService = require("../../../services/userService");

module.exports = {
  async register(req, res) {
    const user = await userService.create(req);

    res.status(200).json({
      status: "OK",
      data: user,
      message: "User Created",
    });
  },
};
